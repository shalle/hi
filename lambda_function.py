import argparse
import os
from urllib.parse import urljoin

import requests


APY_URL = "http://apy.projectjj.com"


def lambda_handler(*_):
    print(_mash(os.environ["TEXT"], "eng", "cat"))


def _mash(text, src_lang, tgt_lang):
    return _call(_call(text, src_lang, tgt_lang), tgt_lang, src_lang)


def _call(text, src_lang, tgt_lang):
    return requests.get(urljoin(APY_URL, "translate"), params={
        "langpair": f"{src_lang}|{tgt_lang}",
        "q": text
    }).json()["responseData"]["translatedText"]
